﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smarTees.Entities
{
   public class baseEntity
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}
