﻿using smarTees.Database;
using smarTees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smarTees.Services
{
    public class categoriesServices
    {
        public void saveCategory(categories category)
        {
            using (var context = new stContext())
            {
                context.category.Add(category);
                context.SaveChanges();
            }
        }
        public void updateCategory(categories category)
        {
            using (var context = new stContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void deleteCategory(int ID)
        {
            using (var context = new stContext())
            {
                var category = context.category.Find(ID);
                //context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                context.category.Remove(category);
                context.SaveChanges();
            }
        }
        public List<categories> getCategories()
        {
            using (var context = new stContext())
            {
                return context.category.ToList();
            }
        }

        public categories getCategory(int ID)
        {
            using (var context = new stContext())
            {
                return context.category.Find(ID);
            }
        }
    }
}
