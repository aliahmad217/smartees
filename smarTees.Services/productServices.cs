﻿using smarTees.Database;
using smarTees.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smarTees.Services
{
    public class productServices
    {
        public void saveProduct(product prod)
        {
            using (var context = new stContext())
            {
                context.products.Add(prod);
                context.SaveChanges();
            }
        }
        public void updateProduct(product prod)
        {
            using (var context = new stContext())
            {
                context.Entry(prod).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void deleteProduct(int ID)
        {
            using (var context = new stContext())
            {
                var product = context.products.Find(ID);
                //context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                context.products.Remove(product);
                context.SaveChanges();
            }
        }
        public List<product> getProduct()
        {
            using (var context = new stContext())
            {
                return context.products.ToList();
            }
        }

        public product getProduct(int ID)
        {
            using (var context = new stContext())
            {
                return context.products.Find(ID);
            }
        }
    }
}