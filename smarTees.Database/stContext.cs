﻿using smarTees.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smarTees.Database
{
   public class stContext : DbContext,IDisposable
    {
       public stContext() : base("smarTeesConnection")
        {

        }
        public DbSet<product> products { get; set; }
        public DbSet<categories> category { get; set; }
    }
}
