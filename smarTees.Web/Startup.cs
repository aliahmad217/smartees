﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(smarTees.Web.Startup))]
namespace smarTees.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
