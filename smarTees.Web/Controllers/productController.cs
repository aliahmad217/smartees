﻿using smarTees.Entities;
using smarTees.Services;
using smarTees.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace smarTees.Web.Controllers
{
    public class productController : Controller
    {
        productServices productService = new productServices();
        
        // GET: product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult productTable(string search)
        {
            var product = productService.getProduct();
            if (string.IsNullOrEmpty(search) == false) {
                product = product.Where(p => p.name != null && p.name.ToLower().Contains(search.ToLower())).ToList();
            }
            
            return PartialView(product);
        }
        [HttpGet]
        public ActionResult Create()
        {
            categoriesServices categoryService = new categoriesServices();
            var categories = categoryService.getCategories();
            return PartialView(categories);
        }
        [HttpPost]
        public ActionResult Create(NewCategoryViewModel model)
        {
            //productService.saveProduct(prod);
            return RedirectToAction("productTable");
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var product = productService.getProduct(ID);
            return PartialView(product);
        }
        [HttpPost]
        public ActionResult Edit(product prod)
        {
            productService.updateProduct(prod);
            return RedirectToAction("productTable");
        }
        //[HttpGet]
        //public ActionResult Delete(int ID)
        //{
        //    var product = productService.getProduct(ID);
        //    return PartialView(product);
        //}
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            //category = categoryService.getCategory(category.ID);
            productService.deleteProduct(ID);
            return RedirectToAction("productTable");
        }
    }
}