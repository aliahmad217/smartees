﻿using smarTees.Entities;
using smarTees.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace smarTees.Web.Controllers
{
    public class categoryController : Controller
    {
        categoriesServices categoryService = new categoriesServices();
        [HttpGet]
        public ActionResult Index()
        {
            var categories =categoryService.getCategories();
            return View(categories);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(categories category)
        {
            categoryService.saveCategory(category);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var category = categoryService.getCategory(ID);
            return View(category);
        }
        [HttpPost]
        public ActionResult Edit(categories category)
        {
            categoryService.updateCategory(category);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            var category = categoryService.getCategory(ID);
            return View(category);
        }
        [HttpPost]
        public ActionResult Delete(categories category)
        {
            //category = categoryService.getCategory(category.ID);
            categoryService.deleteCategory(category.ID);
            return RedirectToAction("Index");
        }
    }
}