﻿using smarTees.Services;
using smarTees.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace smarTees.Web.Controllers
{
    public class HomeController : Controller
    {
        categoriesServices categoryService = new categoriesServices();
        public ActionResult Index()
        {
            HomeViewModels model = new HomeViewModels();
            model.category = categoryService.getCategories();

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}